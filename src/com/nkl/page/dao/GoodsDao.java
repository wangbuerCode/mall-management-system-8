package com.nkl.page.dao;

import java.util.ArrayList;
import java.util.List;

import com.nkl.common.dao.BaseDao;
import com.nkl.page.domain.Goods;
import com.soft.common.util.StringUtil;

public class GoodsDao extends BaseDao {

	public void addGoods(Goods goods){
		super.add(goods);
	}

	public void delGoods(Integer goods_id){
		super.del(Goods.class, goods_id);
	}

	public void delGoodss(String[] goods_ids){
		StringBuilder sBuilder = new StringBuilder();
		for (int i = 0; i <goods_ids.length; i++) {
			sBuilder.append(goods_ids[i]);
			if (i !=goods_ids.length-1) {
				sBuilder.append(",");
			}
		}
		String hql = "DELETE FROM Goods WHERE goods_id IN(" +sBuilder.toString()+")";

		Object[] params = null;

		super.executeUpdateHql(hql, params);
	}

	public void updateGoods(Goods goods){
		Goods _goods = (Goods)super.get(Goods.class, goods.getGoods_id());
		if (goods.getGoods_type_id()!=0) {
			_goods.setGoods_type_id(goods.getGoods_type_id());
		}
		if (!StringUtil.isEmptyString(goods.getGoods_name())) {
			_goods.setGoods_name(goods.getGoods_name());
		}
		if (!StringUtil.isEmptyString(goods.getGoods_pic())) {
			_goods.setGoods_pic(goods.getGoods_pic() );
		}
		if (goods.getGoods_price1()!=0) {
			_goods.setGoods_price1(goods.getGoods_price1() );
		}
		if (goods.getGoods_flag()!=0) {
			_goods.setGoods_flag(goods.getGoods_flag());
		}
		if (goods.getGoods_price2()!=0) {
			_goods.setGoods_price2(goods.getGoods_price2() );
		}
		if (goods.getGoods_count()!=0) {
			_goods.setGoods_count(goods.getGoods_count());
		}
		if (!StringUtil.isEmptyString(goods.getGoods_desc())) {
			_goods.setGoods_desc(goods.getGoods_desc());
		}
		super.update(_goods);
	}

	public void updateGoodsCount(Goods goods){
		Goods _goods = (Goods)super.get(Goods.class, goods.getGoods_id());
		if (goods.getGoods_count()!=-1) {
			_goods.setGoods_count(goods.getGoods_count());
		}
		super.update(_goods);
	}

	@SuppressWarnings("rawtypes")
	public Goods getGoods(Goods goods){
		Goods _goods=null;
		StringBuilder sBuilder = new StringBuilder();
		sBuilder.append("FROM Goods WHERE 1=1");
		List<Object> paramsList = new ArrayList<Object>();
		if (goods.getGoods_id()!=0) {
			sBuilder.append(" and goods_id = ? ");
			paramsList.add(goods.getGoods_id());
		}

		Object[] params = null;
		if (paramsList.size()>0) {
			params = new Object[paramsList.size()];
			for (int i = 0; i < paramsList.size(); i++) {
				params[i] = paramsList.get(i);
			}
		}

		List list = super.executeQueryHql(sBuilder.toString(), params);
		if (list != null && list.size() > 0) {
			_goods = (Goods)list.get(0);
		}

		return _goods;
	}

	@SuppressWarnings("rawtypes")
	public List<Goods>  listGoodss(Goods goods){
		List<Goods> goodss = null;
		StringBuilder sBuilder = new StringBuilder();
		sBuilder.append("FROM Goods WHERE 1=1");
		List<Object> paramsList = new ArrayList<Object>();
		if (goods.getGoods_id()!=0) {
			sBuilder.append(" and goods_id = ? ");
			paramsList.add(goods.getGoods_id());
		}
		if (goods.getGoods_type_id()!=0) {
			sBuilder.append(" and goods_type_id = " + goods.getGoods_type_id() +" ");
		}
		if (goods.getGoods_flag()!=0) {
			sBuilder.append(" and goods_flag = " + goods.getGoods_flag() +" ");
		}

		Object[] params = null;
		if (paramsList.size()>0) {
			params = new Object[paramsList.size()];
			for (int i = 0; i < paramsList.size(); i++) {
				params[i] = paramsList.get(i);
			}
		}

		sBuilder.append(" order by goods_id asc ");

		List list = null;
		if (goods.getStart()!=-1) {
			list = super.findByPageHql(sBuilder.toString(), params, goods.getStart(), goods.getLimit());
		}else {
			list = super.executeQueryHql(sBuilder.toString(), params);
		}
		if (list != null && list.size() > 0) {
			goodss = new ArrayList<Goods>();
			for (Object object : list) {
				goodss.add((Goods)object);
			}
		}

		return goodss;
	}

	@SuppressWarnings("rawtypes")
	public List<Goods>  listGoodssTop(Goods goods){
		List<Goods> goodss = null;
		StringBuilder sBuilder = new StringBuilder();
		sBuilder.append("FROM Goods WHERE 1=1");
		List<Object> paramsList = new ArrayList<Object>();
		if (goods.getGoods_id()!=0) {
			sBuilder.append(" and goods_id = ? ");
			paramsList.add(goods.getGoods_id());
		}
		if (goods.getGoods_type_id()!=0) {
			sBuilder.append(" and goods_type_id = " + goods.getGoods_type_id() +" ");
		}
		if (goods.getGoods_flag()!=0) {
			sBuilder.append(" and goods_flag = " + goods.getGoods_flag() +" ");
		}

		Object[] params = null;
		if (paramsList.size()>0) {
			params = new Object[paramsList.size()];
			for (int i = 0; i < paramsList.size(); i++) {
				params[i] = paramsList.get(i);
			}
		}

		sBuilder.append(" order by goods_date desc,goods_id asc ");

		List list = null;
		if (goods.getStart()!=-1) {
			list = super.findByPageHql(sBuilder.toString(), params, goods.getStart(), goods.getLimit());
		}else {
			list = super.executeQueryHql(sBuilder.toString(), params);
		}
		if (list != null && list.size() > 0) {
			goodss = new ArrayList<Goods>();
			for (Object object : list) {
				goodss.add((Goods)object);
			}
		}

		return goodss;
	}
	
	public int  listGoodssCount(Goods goods){
		int sum = 0;
		StringBuilder sBuilder = new StringBuilder();
		sBuilder.append("SELECT count(*) FROM Goods WHERE 1=1");
		List<Object> paramsList = new ArrayList<Object>();
		if (goods.getGoods_id()!=0) {
			sBuilder.append(" and goods_id = ? ");
			paramsList.add(goods.getGoods_id());
		}
		if (goods.getGoods_type_id()!=0) {
			sBuilder.append(" and goods_type_id = " + goods.getGoods_type_id() +" ");
		}
		if (goods.getGoods_flag()!=0) {
			sBuilder.append(" and goods_flag = " + goods.getGoods_flag() +" ");
		}

		Object[] params = null;
		if (paramsList.size()>0) {
			params = new Object[paramsList.size()];
			for (int i = 0; i < paramsList.size(); i++) {
				params[i] = paramsList.get(i);
			}
		}

		long count = (Long)super.executeQueryCountHql(sBuilder.toString(), params);
		sum = (int)count;
		return sum;
	}

}
