package com.nkl.page.dao;

import java.util.ArrayList;
import java.util.List;

import com.nkl.common.dao.BaseDao;
import com.nkl.page.domain.GoodsType;
import com.soft.common.util.StringUtil;

public class GoodsTypeDao extends BaseDao {

	public void addGoodsType(GoodsType goodsType){
		super.add(goodsType);
	}

	public void delGoodsType(Integer goods_type_id){
		super.del(GoodsType.class, goods_type_id);
	}

	public void delGoodsTypes(String[] goods_type_ids){
		StringBuilder sBuilder = new StringBuilder();
		for (int i = 0; i <goods_type_ids.length; i++) {
			sBuilder.append(goods_type_ids[i]);
			if (i !=goods_type_ids.length-1) {
				sBuilder.append(",");
			}
		}
		String hql = "DELETE FROM GoodsType WHERE goods_type_id IN(" +sBuilder.toString()+")";

		Object[] params = null;

		super.executeUpdateHql(hql, params);
	}

	public void updateGoodsType(GoodsType goodsType){
		GoodsType _goodsType = (GoodsType)super.get(GoodsType.class, goodsType.getGoods_type_id());
		if (!StringUtil.isEmptyString(goodsType.getGoods_type_name())) {
			_goodsType.setGoods_type_name(goodsType.getGoods_type_name());
		}
		super.update(_goodsType);
	}

	@SuppressWarnings("rawtypes")
	public GoodsType getGoodsType(GoodsType goodsType){
		GoodsType _goodsType=null;
		StringBuilder sBuilder = new StringBuilder();
		sBuilder.append("FROM GoodsType WHERE 1=1");
		List<Object> paramsList = new ArrayList<Object>();
		if (goodsType.getGoods_type_id()!=0) {
			sBuilder.append(" and goods_type_id = ? ");
			paramsList.add(goodsType.getGoods_type_id());
		}
		if (!StringUtil.isEmptyString(goodsType.getGoods_type_name())) {
			sBuilder.append(" and goods_type_name = '" + goodsType.getGoods_type_name() +"' ");
		}

		Object[] params = null;
		if (paramsList.size()>0) {
			params = new Object[paramsList.size()];
			for (int i = 0; i < paramsList.size(); i++) {
				params[i] = paramsList.get(i);
			}
		}

		List list = super.executeQueryHql(sBuilder.toString(), params);
		if (list != null && list.size() > 0) {
			_goodsType = (GoodsType)list.get(0);
		}

		return _goodsType;
	}

	@SuppressWarnings("rawtypes")
	public List<GoodsType>  listGoodsTypes(GoodsType goodsType){
		List<GoodsType> goodsTypes = null;
		StringBuilder sBuilder = new StringBuilder();
		sBuilder.append("FROM GoodsType WHERE 1=1");
		List<Object> paramsList = new ArrayList<Object>();
		if (goodsType.getGoods_type_id()!=0) {
			sBuilder.append(" and goods_type_id = ? ");
			paramsList.add(goodsType.getGoods_type_id());
		}
		if (!StringUtil.isEmptyString(goodsType.getGoods_type_name())) {
			sBuilder.append(" and goods_type_name like '%" + goodsType.getGoods_type_name() +"%' ");
		}

		Object[] params = null;
		if (paramsList.size()>0) {
			params = new Object[paramsList.size()];
			for (int i = 0; i < paramsList.size(); i++) {
				params[i] = paramsList.get(i);
			}
		}

		sBuilder.append(" order by goods_type_id asc ");

		List list = null;
		if (goodsType.getStart()!=-1) {
			list = super.findByPageHql(sBuilder.toString(), params, goodsType.getStart(), goodsType.getLimit());
		}else {
			list = super.executeQueryHql(sBuilder.toString(), params);
		}
		if (list != null && list.size() > 0) {
			goodsTypes = new ArrayList<GoodsType>();
			for (Object object : list) {
				goodsTypes.add((GoodsType)object);
			}
		}

		return goodsTypes;
	}

	public int  listGoodsTypesCount(GoodsType goodsType){
		int sum = 0;
		StringBuilder sBuilder = new StringBuilder();
		sBuilder.append("SELECT count(*) FROM GoodsType WHERE 1=1");
		List<Object> paramsList = new ArrayList<Object>();
		if (goodsType.getGoods_type_id()!=0) {
			sBuilder.append(" and goods_type_id = ? ");
			paramsList.add(goodsType.getGoods_type_id());
		}
		if (!StringUtil.isEmptyString(goodsType.getGoods_type_name())) {
			sBuilder.append(" and goods_type_name like '%" + goodsType.getGoods_type_name() +"%' ");
		}

		Object[] params = null;
		if (paramsList.size()>0) {
			params = new Object[paramsList.size()];
			for (int i = 0; i < paramsList.size(); i++) {
				params[i] = paramsList.get(i);
			}
		}

		long count = (Long)super.executeQueryCountHql(sBuilder.toString(), params);
		sum = (int)count;
		return sum;
	}

}
