package com.nkl.page.dao;

import java.util.ArrayList;
import java.util.List;

import com.nkl.common.dao.BaseDao;
import com.nkl.page.domain.Evaluate;
import com.soft.common.util.StringUtil;

public class EvaluateDao extends BaseDao  {

	public void addEvaluate(Evaluate evaluate){
		super.add(evaluate);
	}

	public void delEvaluate(Integer evaluate_id){
		super.del(Evaluate.class, evaluate_id);
	}

	public void delEvaluates(String[] evaluate_ids){
		StringBuilder sBuilder = new StringBuilder();
		for (int i = 0; i <evaluate_ids.length; i++) {
			sBuilder.append(evaluate_ids[i]);
			if (i !=evaluate_ids.length-1) {
				sBuilder.append(",");
			}
		}
		String hql = "DELETE FROM Evaluate WHERE evaluate_id IN(" +sBuilder.toString()+")";

		Object[] params = null;

		super.executeUpdateHql(hql, params);
	}
	
	public void addEvaluateBatch(Evaluate evaluate){
		String sql = "INSERT INTO evaluate(orders_no,user_id,nick_name,goods_id,evaluate_date,evaluate_level,evaluate_content) "
				   + " SELECT  '"+evaluate.getOrders_no()+"',"+evaluate.getUser_id()+",'"+evaluate.getNick_name()+"',goods_id,'"
				   + evaluate.getEvaluate_dateDesc()+"',"+ evaluate.getEvaluate_level()+",'"+evaluate.getEvaluate_content()
				   + "'  FROM orders_detail WHERE orders_no='"+evaluate.getOrders_no()+"'";
		Object[] params = null;
		super.executeUpdateSql(sql, params);
	}
	
	public void updateEvaluate(Evaluate evaluate){
		Evaluate _evaluate = (Evaluate)super.get(Evaluate.class, evaluate.getEvaluate_id());
		if (evaluate.getEvaluate_level()!=0) {
			_evaluate.setEvaluate_level(evaluate.getEvaluate_level());
		}
		super.update(_evaluate);
	}

	@SuppressWarnings("rawtypes")
	public Evaluate getEvaluate(Evaluate evaluate){
		Evaluate _evaluate=null;
		StringBuilder sBuilder = new StringBuilder();
		sBuilder.append("FROM Evaluate WHERE 1=1");
		List<Object> paramsList = new ArrayList<Object>();
		if (evaluate.getEvaluate_id()!=0) {
			sBuilder.append(" and evaluate_id = ? ");
			paramsList.add(evaluate.getEvaluate_id());
		}
		if (!StringUtil.isEmptyString(evaluate.getOrders_no())) {
			sBuilder.append(" and orders_no = '" + evaluate.getOrders_no() +"' ");
		}
		if (evaluate.getUser_id()!=0) {
			sBuilder.append(" and user_id = " + evaluate.getUser_id() +" ");
		}

		Object[] params = null;
		if (paramsList.size()>0) {
			params = new Object[paramsList.size()];
			for (int i = 0; i < paramsList.size(); i++) {
				params[i] = paramsList.get(i);
			}
		}

		List list = super.executeQueryHql(sBuilder.toString(), params);
		if (list != null && list.size() > 0) {
			_evaluate = (Evaluate)list.get(0);
		}

		return _evaluate;
	}

	@SuppressWarnings("rawtypes")
	public List<Evaluate>  listEvaluates(Evaluate evaluate){
		List<Evaluate> evaluates = null;
		StringBuilder sBuilder = new StringBuilder();
		sBuilder.append("FROM Evaluate WHERE 1=1");
		List<Object> paramsList = new ArrayList<Object>();
		if (evaluate.getEvaluate_id()!=0) {
			sBuilder.append(" and evaluate_id = ? ");
			paramsList.add(evaluate.getEvaluate_id());
		}
		if (!StringUtil.isEmptyString(evaluate.getOrders_no())) {
			sBuilder.append(" and orders_no = '" + evaluate.getOrders_no() +"' ");
		}
		if (evaluate.getUser_id()!=0) {
			sBuilder.append(" and user_id = " + evaluate.getUser_id() +" ");
		}
		if (evaluate.getGoods_id()!=0) {
			sBuilder.append(" and goods_id = " + evaluate.getGoods_id() +" ");
		}

		Object[] params = null;
		if (paramsList.size()>0) {
			params = new Object[paramsList.size()];
			for (int i = 0; i < paramsList.size(); i++) {
				params[i] = paramsList.get(i);
			}
		}

		sBuilder.append(" order by evaluate_date desc,evaluate_id asc ");

		List list = null;
		if (evaluate.getStart()!=-1) {
			list = super.findByPageHql(sBuilder.toString(), params, evaluate.getStart(), evaluate.getLimit());
		}else {
			list = super.executeQueryHql(sBuilder.toString(), params);
		}
		if (list != null && list.size() > 0) {
			evaluates = new ArrayList<Evaluate>();
			for (Object object : list) {
				evaluates.add((Evaluate)object);
			}
		}

		return evaluates;
	}

	public int  listEvaluatesCount(Evaluate evaluate){
		int sum = 0;
		StringBuilder sBuilder = new StringBuilder();
		sBuilder.append("SELECT count(*) FROM Evaluate WHERE 1=1");
		List<Object> paramsList = new ArrayList<Object>();
		if (evaluate.getEvaluate_id()!=0) {
			sBuilder.append(" and evaluate_id = ? ");
			paramsList.add(evaluate.getEvaluate_id());
		}
		if (!StringUtil.isEmptyString(evaluate.getOrders_no())) {
			sBuilder.append(" and orders_no = '" + evaluate.getOrders_no() +"' ");
		}
		if (evaluate.getUser_id()!=0) {
			sBuilder.append(" and user_id = " + evaluate.getUser_id() +" ");
		}
		if (evaluate.getGoods_id()!=0) {
			sBuilder.append(" and goods_id = " + evaluate.getGoods_id() +" ");
		}

		Object[] params = null;
		if (paramsList.size()>0) {
			params = new Object[paramsList.size()];
			for (int i = 0; i < paramsList.size(); i++) {
				params[i] = paramsList.get(i);
			}
		}

		long count = (Long)super.executeQueryCountHql(sBuilder.toString(), params);
		sum = (int)count;
		return sum;
	}

}
