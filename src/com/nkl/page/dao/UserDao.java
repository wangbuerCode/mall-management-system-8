package com.nkl.page.dao;

import java.util.ArrayList;
import java.util.List;

import com.nkl.common.dao.BaseDao;
import com.nkl.page.domain.User;
import com.soft.common.util.StringUtil;

public class UserDao extends BaseDao {

	public void addUser(User user){
		super.add(user);
	}

	public void delUser(Integer user_id){
		super.del(User.class, user_id);
	}

	public void delUsers(String[] user_ids){
		StringBuilder sBuilder = new StringBuilder();
		for (int i = 0; i <user_ids.length; i++) {
			sBuilder.append(user_ids[i]);
			if (i !=user_ids.length-1) {
				sBuilder.append(",");
			}
		}
		String hql = "DELETE FROM User WHERE user_id IN(" +sBuilder.toString()+")";

		Object[] params = null;

		super.executeUpdateHql(hql, params);
	}


	public void updateUser(User user){
		User _user = (User)super.get(User.class, user.getUser_id());
		if (!StringUtil.isEmptyString(user.getUser_pass())) {
			_user.setUser_pass(user.getUser_pass());
		}
		if (!StringUtil.isEmptyString(user.getReal_name())) {
			_user.setReal_name(user.getReal_name() );
		}
		if (user.getUser_sex()!=0) {
			_user.setUser_sex(user.getUser_sex());
		}
		if (!StringUtil.isEmptyString(user.getNick_name())) {
			_user.setNick_name(user.getNick_name() );
		}
		if (!StringUtil.isEmptyString(user.getUser_address())) {
			_user.setUser_address(user.getUser_address() );
		}
		if (!StringUtil.isEmptyString(user.getUser_mail())) {
			_user.setUser_mail(user.getUser_mail() );
		}
		if (!StringUtil.isEmptyString(user.getUser_phone())) {
			_user.setUser_phone(user.getUser_phone() );
		}
		if (user.getUser_flag()!=0) {
			_user.setUser_flag(user.getUser_flag());
		}
		super.update(_user);
	}

	@SuppressWarnings("rawtypes")
	public User getUser(User user){
		User _user=null;
		StringBuilder sBuilder = new StringBuilder();
		sBuilder.append("FROM User WHERE 1=1");
		List<Object> paramsList = new ArrayList<Object>();
		if (user.getUser_id()!=0) {
			sBuilder.append(" and user_id = ? ");
			paramsList.add(user.getUser_id());
		}
		if (!StringUtil.isEmptyString(user.getUser_pass())) {
			sBuilder.append(" and user_pass ='" + user.getUser_pass() +"' ");
		}
		if (!StringUtil.isEmptyString(user.getUser_name())) {
			sBuilder.append(" and user_name ='" + user.getUser_name() +"' ");
		}
		if (!StringUtil.isEmptyString(user.getUser_types())) {
			sBuilder.append(" and user_type in (" + user.getUser_types() +") ");
		}
		if (user.getUser_type()!=0) {
			sBuilder.append(" and user_type =" + user.getUser_type() +" ");
		}

		Object[] params = null;
		if (paramsList.size()>0) {
			params = new Object[paramsList.size()];
			for (int i = 0; i < paramsList.size(); i++) {
				params[i] = paramsList.get(i);
			}
		}

		List list = super.executeQueryHql(sBuilder.toString(), params);
		if (list != null && list.size() > 0) {
			_user = (User)list.get(0);
		}

		return _user;
	}

	@SuppressWarnings("rawtypes")
	public List<User>  listUsers(User user){
		List<User> users = null;
		StringBuilder sBuilder = new StringBuilder();
		sBuilder.append("FROM User WHERE 1=1");
		List<Object> paramsList = new ArrayList<Object>();
		if (user.getUser_id()!=0) {
			sBuilder.append(" and user_id = ? ");
			paramsList.add(user.getUser_id());
		}
		if (!StringUtil.isEmptyString(user.getUser_name())) {
			sBuilder.append(" and user_name like '%" + user.getUser_name() +"%' ");
		}
		if (!StringUtil.isEmptyString(user.getReal_name())) {
			sBuilder.append(" and real_name like '%" + user.getReal_name() +"%' ");
		}
		if (!StringUtil.isEmptyString(user.getNick_name())) {
			sBuilder.append(" and nick_name like '%" + user.getNick_name() +"%' ");
		}
		if (user.getUser_flag()!=0) {
			sBuilder.append(" and user_flag =" + user.getUser_flag() +" ");
		}
		if (user.getUser_type()!=0) {
			sBuilder.append(" and user_type =" + user.getUser_type() +" ");
		}
		if (!StringUtil.isEmptyString(user.getUser_types())) {
			sBuilder.append(" and user_type in (" + user.getUser_types() +") ");
		}

		Object[] params = null;
		if (paramsList.size()>0) {
			params = new Object[paramsList.size()];
			for (int i = 0; i < paramsList.size(); i++) {
				params[i] = paramsList.get(i);
			}
		}

		sBuilder.append(" order by user_id asc ");

		List list = null;
		if (user.getStart()!=-1) {
			list = super.findByPageHql(sBuilder.toString(), params, user.getStart(), user.getLimit());
		}else {
			list = super.executeQueryHql(sBuilder.toString(), params);
		}
		if (list != null && list.size() > 0) {
			users = new ArrayList<User>();
			for (Object object : list) {
				users.add((User)object);
			}
		}

		return users;
	}

	public int  listUsersCount(User user){
		int sum = 0;
		StringBuilder sBuilder = new StringBuilder();
		sBuilder.append("SELECT count(*) FROM User WHERE 1=1");
		List<Object> paramsList = new ArrayList<Object>();
		if (user.getUser_id()!=0) {
			sBuilder.append(" and user_id = ? ");
			paramsList.add(user.getUser_id());
		}
		if (!StringUtil.isEmptyString(user.getUser_name())) {
			sBuilder.append(" and user_name like '%" + user.getUser_name() +"%' ");
		}
		if (!StringUtil.isEmptyString(user.getReal_name())) {
			sBuilder.append(" and real_name like '%" + user.getReal_name() +"%' ");
		}
		if (!StringUtil.isEmptyString(user.getNick_name())) {
			sBuilder.append(" and nick_name like '%" + user.getNick_name() +"%' ");
		}
		if (user.getUser_flag()!=0) {
			sBuilder.append(" and user_flag =" + user.getUser_flag() +" ");
		}
		if (user.getUser_type()!=0) {
			sBuilder.append(" and user_type =" + user.getUser_type() +" ");
		}
		if (!StringUtil.isEmptyString(user.getUser_types())) {
			sBuilder.append(" and user_type in (" + user.getUser_types() +") ");
		}

		Object[] params = null;
		if (paramsList.size()>0) {
			params = new Object[paramsList.size()];
			for (int i = 0; i < paramsList.size(); i++) {
				params[i] = paramsList.get(i);
			}
		}

		long count = (Long)super.executeQueryCountHql(sBuilder.toString(), params);
		sum = (int)count;
		return sum;
	}

}
