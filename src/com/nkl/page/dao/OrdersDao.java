package com.nkl.page.dao;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import com.nkl.page.domain.Orders;
import com.nkl.common.dao.BaseDao;
import com.soft.common.util.StringUtil;

public class OrdersDao extends BaseDao {

	public void addOrders(Orders orders){
		super.add(orders);
	}

	public void delOrders(Integer orders_id){
		super.del(Orders.class, orders_id);
	}

	public void delOrderss(String[] orders_ids){
		StringBuilder sBuilder = new StringBuilder();
		for (int i = 0; i <orders_ids.length; i++) {
			sBuilder.append(orders_ids[i]);
			if (i !=orders_ids.length-1) {
				sBuilder.append(",");
			}
		}
		String hql = "DELETE FROM Orders WHERE orders_id IN(" +sBuilder.toString()+")";

		Object[] params = null;

		super.executeUpdateHql(hql, params);
	}

	public void updateOrders(Orders orders){
		Orders _orders = getOrders(orders);
		if (!StringUtil.isEmptyString(orders.getReal_name())) {
			_orders.setReal_name(orders.getReal_name());
		}
		if (!StringUtil.isEmptyString(orders.getUser_address())) {
			_orders.setReal_name(orders.getUser_address());
		}
		if (!StringUtil.isEmptyString(orders.getUser_phone())) {
			_orders.setUser_phone(orders.getUser_phone());
		}
		if (orders.getOrders_flag()!=0) {
			_orders.setOrders_flag(orders.getOrders_flag());
		}
		if (orders.getSend_id()!=0) {
			_orders.setSend_id(orders.getSend_id());
		}
		if (!StringUtil.isEmptyString(orders.getSend_name())) {
			_orders.setSend_name(orders.getSend_name());
		}
		super.update(_orders);
	}
	
	public void updateOrdersMoney(Orders orders, Connection conn){
		StringBuilder sBuilder = new StringBuilder();
		sBuilder.append("UPDATE orders SET orders_money = (select sum(d.goods_money) from orders_detail d where d.orders_no="+orders.getOrders_no()+") ");
		sBuilder.append(" where orders_no = '" + orders.getOrders_no() +"' ");

		Object[] params = null;
		super.executeUpdateSql(sBuilder.toString(), params);
	}
	
	@SuppressWarnings("rawtypes")
	public Orders getOrders(Orders orders){
		Orders _orders=null;
		StringBuilder sBuilder = new StringBuilder();
		sBuilder.append("FROM Orders WHERE 1=1");
		List<Object> paramsList = new ArrayList<Object>();
		if (orders.getOrders_id()!=0) {
			sBuilder.append(" and orders_id = ? ");
			paramsList.add(orders.getOrders_id());
		}
		if (!StringUtil.isEmptyString(orders.getOrders_no())) {
			sBuilder.append(" and orders_no = '" + orders.getOrders_no() +"' ");
		}

		Object[] params = null;
		if (paramsList.size()>0) {
			params = new Object[paramsList.size()];
			for (int i = 0; i < paramsList.size(); i++) {
				params[i] = paramsList.get(i);
			}
		}

		List list = super.executeQueryHql(sBuilder.toString(), params);
		if (list != null && list.size() > 0) {
			_orders = (Orders)list.get(0);
		}

		return _orders;
	}

	@SuppressWarnings("rawtypes")
	public List<Orders>  listOrderss(Orders orders){
		List<Orders> orderss = null;
		StringBuilder sBuilder = new StringBuilder();
		sBuilder.append("FROM Orders o WHERE 1=1");
		List<Object> paramsList = new ArrayList<Object>();
		if (orders.getOrders_id()!=0) {
			sBuilder.append(" and orders_id = ? ");
			paramsList.add(orders.getOrders_id());
		}
		if (orders.getUser_id()!=0) {
			sBuilder.append(" and o.user_id = " + orders.getUser_id() +" ");
		}
		if (!StringUtil.isEmptyString(orders.getReal_name())) {
			sBuilder.append(" and o.real_name like '%" + orders.getReal_name() +"%' ");
		}
		if (!StringUtil.isEmptyString(orders.getOrders_no())) {
			sBuilder.append(" and o.orders_no like '%" + orders.getOrders_no() +"%' ");
		}
		if (orders.getOrders_date_min()!=null) {
			sBuilder.append(" and o.orders_date >= ? ");
			paramsList.add(orders.getOrders_date_min());
		}
		if (orders.getOrders_date_max()!=null) {
			sBuilder.append(" and o.orders_date <= ? ");
			paramsList.add(orders.getOrders_date_max());
		}
		if (orders.getSend_id()!=0) {
			sBuilder.append(" and o.send_id = " + orders.getSend_id() +" ");
		}
		if (!StringUtil.isEmptyString(orders.getSend_name())) {
			sBuilder.append(" and o.send_name like '%" + orders.getSend_name() +"%' ");
		}
		if (orders.getOrders_flag()!=0) {
			sBuilder.append(" and o.orders_flag = " + orders.getOrders_flag() +" ");
		}

		Object[] params = null;
		if (paramsList.size()>0) {
			params = new Object[paramsList.size()];
			for (int i = 0; i < paramsList.size(); i++) {
				params[i] = paramsList.get(i);
			}
		}

		sBuilder.append(" order by o.orders_date desc,orders_id asc ");

		List list = null;
		if (orders.getStart()!=-1) {
			list = super.findByPageHql(sBuilder.toString(), params, orders.getStart(), orders.getLimit());
		}else {
			list = super.executeQueryHql(sBuilder.toString(), params);
		}
		if (list != null && list.size() > 0) {
			orderss = new ArrayList<Orders>();
			for (Object object : list) {
				orderss.add((Orders)object);
			}
		}

		return orderss;
	}

	public int  listOrderssCount(Orders orders){
		int sum = 0;
		StringBuilder sBuilder = new StringBuilder();
		sBuilder.append("SELECT count(*) FROM Orders o WHERE 1=1");
		List<Object> paramsList = new ArrayList<Object>();
		if (orders.getOrders_id()!=0) {
			sBuilder.append(" and orders_id = ? ");
			paramsList.add(orders.getOrders_id());
		}
		if (orders.getUser_id()!=0) {
			sBuilder.append(" and o.user_id = " + orders.getUser_id() +" ");
		}
		if (!StringUtil.isEmptyString(orders.getReal_name())) {
			sBuilder.append(" and o.real_name like '%" + orders.getReal_name() +"%' ");
		}
		if (!StringUtil.isEmptyString(orders.getOrders_no())) {
			sBuilder.append(" and o.orders_no like '%" + orders.getOrders_no() +"%' ");
		}
		if (orders.getOrders_date_min()!=null) {
			sBuilder.append(" and o.orders_date >= ? ");
			paramsList.add(orders.getOrders_date_min());
		}
		if (orders.getOrders_date_max()!=null) {
			sBuilder.append(" and o.orders_date <= ? ");
			paramsList.add(orders.getOrders_date_max());
		}
		if (orders.getSend_id()!=0) {
			sBuilder.append(" and o.send_id = " + orders.getSend_id() +" ");
		}
		if (!StringUtil.isEmptyString(orders.getSend_name())) {
			sBuilder.append(" and o.send_name like '%" + orders.getSend_name() +"%' ");
		}
		if (orders.getOrders_flag()!=0) {
			sBuilder.append(" and o.orders_flag = " + orders.getOrders_flag() +" ");
		}

		Object[] params = null;
		if (paramsList.size()>0) {
			params = new Object[paramsList.size()];
			for (int i = 0; i < paramsList.size(); i++) {
				params[i] = paramsList.get(i);
			}
		}

		long count = (Long)super.executeQueryCountHql(sBuilder.toString(), params);
		sum = (int)count;
		return sum;
	}

}
