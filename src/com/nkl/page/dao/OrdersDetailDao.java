package com.nkl.page.dao;

import java.util.ArrayList;
import java.util.List;

import com.nkl.common.dao.BaseDao;
import com.nkl.page.domain.OrdersDetail;
import com.soft.common.util.StringUtil;

public class OrdersDetailDao extends BaseDao {

	public void addOrdersDetail(OrdersDetail ordersDetail){
		super.add(ordersDetail);
	}

	public void delOrdersDetail(Integer detail_id){
		super.del(OrdersDetail.class, detail_id);
	}

	public void delOrdersDetails(String[] detail_ids){
		StringBuilder sBuilder = new StringBuilder();
		for (int i = 0; i <detail_ids.length; i++) {
			sBuilder.append(detail_ids[i]);
			if (i !=detail_ids.length-1) {
				sBuilder.append(",");
			}
		}
		String hql = "DELETE FROM OrdersDetail WHERE detail_id IN(" +sBuilder.toString()+")";

		Object[] params = null;

		super.executeUpdateHql(hql, params);
	}

	public void updateOrdersDetail(OrdersDetail ordersDetail){
		OrdersDetail _ordersDetail = (OrdersDetail)super.get(OrdersDetail.class, ordersDetail.getDetail_id());
		if (ordersDetail.getGoods_price()!=0) {
			_ordersDetail.setGoods_price(ordersDetail.getGoods_price());
		}
		if (ordersDetail.getGoods_count()!=0) {
			_ordersDetail.setGoods_count(ordersDetail.getGoods_count());
		}
		if (ordersDetail.getGoods_money()!=0) {
			_ordersDetail.setGoods_money(ordersDetail.getGoods_money());
		}
		super.update(_ordersDetail);
	}

	@SuppressWarnings("rawtypes")
	public OrdersDetail getOrdersDetail(OrdersDetail ordersDetail){
		OrdersDetail _ordersDetail=null;
		StringBuilder sBuilder = new StringBuilder();
		sBuilder.append("FROM OrdersDetail WHERE 1=1");
		List<Object> paramsList = new ArrayList<Object>();
		if (ordersDetail.getDetail_id()!=0) {
			sBuilder.append(" and detail_id = ? ");
			paramsList.add(ordersDetail.getDetail_id());
		}

		Object[] params = null;
		if (paramsList.size()>0) {
			params = new Object[paramsList.size()];
			for (int i = 0; i < paramsList.size(); i++) {
				params[i] = paramsList.get(i);
			}
		}

		List list = super.executeQueryHql(sBuilder.toString(), params);
		if (list != null && list.size() > 0) {
			_ordersDetail = (OrdersDetail)list.get(0);
		}

		return _ordersDetail;
	}

	@SuppressWarnings("rawtypes")
	public List<OrdersDetail>  listOrdersDetails(OrdersDetail ordersDetail){
		List<OrdersDetail> ordersDetails = null;
		StringBuilder sBuilder = new StringBuilder();
		sBuilder.append("FROM OrdersDetail WHERE 1=1");
		List<Object> paramsList = new ArrayList<Object>();
		if (ordersDetail.getDetail_id()!=0) {
			sBuilder.append(" and detail_id = ? ");
			paramsList.add(ordersDetail.getDetail_id());
		}
		if (!StringUtil.isEmptyString(ordersDetail.getOrders_no())) {
			sBuilder.append(" and orders_no like '%" + ordersDetail.getOrders_no() +"%' ");
		}

		Object[] params = null;
		if (paramsList.size()>0) {
			params = new Object[paramsList.size()];
			for (int i = 0; i < paramsList.size(); i++) {
				params[i] = paramsList.get(i);
			}
		}


		sBuilder.append(" order by detail_id asc ");

		List list = null;
		if (ordersDetail.getStart()!=-1) {
			list = super.findByPageHql(sBuilder.toString(), params, ordersDetail.getStart(), ordersDetail.getLimit());
		}else {
			list = super.executeQueryHql(sBuilder.toString(), params);
		}
		if (list != null && list.size() > 0) {
			ordersDetails = new ArrayList<OrdersDetail>();
			for (Object object : list) {
				ordersDetails.add((OrdersDetail)object);
			}
		}

		return ordersDetails;
	}

	public int  listOrdersDetailsCount(OrdersDetail ordersDetail){
		int sum = 0;
		StringBuilder sBuilder = new StringBuilder();
		sBuilder.append("SELECT count(*) FROM OrdersDetail WHERE 1=1");
		List<Object> paramsList = new ArrayList<Object>();
		if (ordersDetail.getDetail_id()!=0) {
			sBuilder.append(" and detail_id = ? ");
			paramsList.add(ordersDetail.getDetail_id());
		}
		if (!StringUtil.isEmptyString(ordersDetail.getOrders_no())) {
			sBuilder.append(" and orders_no like '%" + ordersDetail.getOrders_no() +"%' ");
		}

		Object[] params = null;
		if (paramsList.size()>0) {
			params = new Object[paramsList.size()];
			for (int i = 0; i < paramsList.size(); i++) {
				params[i] = paramsList.get(i);
			}
		}

		long count = (Long)super.executeQueryCountHql(sBuilder.toString(), params);
		sum = (int)count;
		return sum;
	}

}
